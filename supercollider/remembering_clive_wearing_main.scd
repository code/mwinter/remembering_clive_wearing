(
// MAIN LAUNCH
/*
Loads necessary files and definitions
When starting over, best to reboot server and intepreter

No GUI, just keycodes:
<ctrl + f> = enter full screen, <esc> = exit full screen
<ctrl + r> = run synth, <ctrl + p> = pause synth
<ctrl + s> = trigger start / fade in, <ctrl + e> = trigger end / fade out, <ctrl + t> = trigger wake

When ~arduino == 1 (true), the app assumes that an arduino is reachable, otherwise errors will be thrown

When automate == 1 (true), the trigger keys are disabled and the following lists in the SynthDef are used:
startTimes, wakeTimes, and endTimes

This launches the synth in a paused state to give the user time to put the visuals in full screen.
To start the synth, <ctrl + r> must be executed
*/

var dir = thisProcess.nowExecutingPath.dirname;
var automate = 1;
~arduino = 1;
~fadeInTrigBus = Bus.control(s); ~wakeTrigBus = Bus.control(s); ~fadeOutTrigBus = Bus.control(s);
"remembering_clive_wearing_visuals.scd".loadRelative(true, {
	"remembering_clive_wearing_synthdef.scd".loadRelative(true, {
		var fileCount = 0, samples;
		PathName(dir +/+ "../audio").files.postln;
		samples = PathName(dir +/+ "../audio").files.sort({arg a, b;
			a.fileName.toLower < b.fileName.toLower }).collect({|file|
			Buffer.read(s, file.fullPath, action: {
				if(fileCount == 8, {~flicker = Synth.newPaused(\flicker,
					[\automate, automate, \bufs, samples,
						\fadeInTrigBusNum, ~fadeInTrigBus,
						\fadeOutTrigBusNum, ~fadeOutTrigBus,
						\wakeTrigBusNum, ~wakeTrigBus])}, {});
				fileCount = fileCount + 1;
			});
		});
	});
});
)