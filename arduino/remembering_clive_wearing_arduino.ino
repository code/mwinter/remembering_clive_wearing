
#include <TimerOne.h> // Include Timer1 library

// Pin outputs for the 18 lights
int AC_pin_offsets[18] = {22, 24, 26, 28, 30, 23, 25, 27, 29, 32, 34, 36, 38, 31, 33, 35, 37, 39};
int freq = 50; // 50 for EU and 60 for US 
int dims[18]; // dimmer values
unsigned char clock_tick; // variable for Timer1
char incomingByte;
int val = 0;
int light = 0;
int delim = 0;

void setup() {
  Serial.begin(115200);
  for(int i = 0; i < 18; i++){
    dims[i] = 215;
    pinMode(AC_pin_offsets[i], OUTPUT);   
  };
  attachInterrupt(0, zero_crosss_int, RISING);
  Timer1.initialize(1000000 / (freq * 2) / 256); // 8 bit control 
  Timer1.attachInterrupt(timerIsr); // attach the service routine here
}

// this is the dimmer
void timerIsr() {
  clock_tick++;
  for(int i = 0; i < 18; i++) {
    if (dims[i] == clock_tick) {
      digitalWrite(AC_pin_offsets[i], HIGH); // triac on
      delayMicroseconds(10); // triac On propogation delay (for 60Hz use 8.33, for 50Hz use 10)
      digitalWrite(AC_pin_offsets[i], LOW); //triac off
      }
  };
}

// function to be fired at the zero crossing to dim the light
void zero_crosss_int() {
  clock_tick=0;
}

void loop(){
  if (Serial.available() > 0) {   // something came across serial
    while(1) { 
      incomingByte = Serial.read();
      if(incomingByte == -1) {break;};
      if(incomingByte < 0) {val = 256 + incomingByte;} else {val = incomingByte;};
      if(val == 253) {delim = 1; continue;};
      if(val == 254) {delim = 0; continue;};
      if(delim == 0) light = val;
      if(delim == 1) dims[light] = val;
    }
  }  
}
